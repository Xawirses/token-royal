Token Royal
================

Author
----------------
- Michael Gileta : 'The Elf'
- Raphael Kumar : 'The Barbarian'
- Kevin Duglue : 'The Minstrel'
- Julien Jandot : 'The Dwarf'

Description
----------------
Token Royal is a game like a 'Catch me if you can'.

The game principle is to catch the other tokens by moving on their position(like chess game).

Compilation Directive
----------------
```
g++ -Wall -std=c++11 [-o TokenRoyal] GameMain.cxx
```
