/**
*
*\file GameMain.cxx
*\author Gileta Michael, Raphael Kumar, Kevin Duglue, Julien Jandot
*\date 01/01/13
*\brief Main for Token Royal Game
*
**/

#include <cctype>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <time.h>
#include <vector>

using namespace std;

/**
 *\namespace nsGame
 *\brief Namespace for the Main function of game
 */
namespace nsGame
{
    typedef vector <char> CVChar; /*!< Type representing a char vector */
    typedef vector <CVChar> CVMatrix; /*!< Type representing char matrix */
    typedef pair <unsigned, unsigned> CPosition; /*!< Type representing a position in the grid */
    typedef vector <CPosition> CVPosition; /*!< Type representing all players' position in the grid */

    // Map Symbol
    const char KEmpty = ' '; /*!< Constant representing an empty cell */
    const char KWall = '#'; /*!< Constant representing a wall cell */
    
    // Players Informations
    unsigned NbPlayer = 4; /*!< Value representing default number of player */
    unsigned NbPlayerAlive = 4; /*!< Value representing default number of player who aren't dead */
    vector <bool> VPlayerDead (4, true); /*!< Vector representing the death of a player */
    
    // Players Symbol & co
    vector <char> SymbolPlayer {'X','O', 'Y', 'T'}; /*!< Vector representing the form of players */
    vector <string> ColorPlayer {"31", "34", "35", "32"}; /*!< Vector representing the color of players */
    vector <string> ColorNamePlayer {"Red", "Blue", "Magenta", "Green"}; /*!< Vector representing the default name color of players */
    
    // Players Keys
    const vector <string> KVNomTouch {"Top-left", "Top", "Top-right", "Left", "Right", "Bottom-left", "Bottom", "Bottom-right"}; /*!< Constant representing the different movement */
    CVChar VKey {'a','z','e','q','d', 'w', 'x', 'c'}; /*!< Vector representing the default movement for a player */
    CVMatrix VPlayersKey (4, VKey); /*!< Matrix representing the default movement for all players */
      
    // Font color (+10 for background color)
    const string KReset = "0"; /*!< Default color */
    const string KBlack = "30"; /*!< Black color */
    const string KRed = "31"; /*!< Red color */
    const string KGreen = "32"; /*!< Green color */
    const string KYellow = "33"; /*!< Yellow color */
    const string KBlue = "34"; /*!< Blue color */
    const string KMagenta = "35"; /*!< Magenta color*/
    const string KCyan = "36"; /*!< Cyan color*/

    //Map Control
    unsigned NbColumn = 10; /*!< Number of column of the matrix of the game */
    unsigned NbLine = 10; /*!< Number of line of the matrix of the game */

    /**
    * \fn Color (const string & Color)
    * \brief Function that changes the font color of the console.
    * \param Color Selected color
    */
    void Color (const string & Color)
    {
        cout << "\033[" << Color <<"m";
    } // Color ()

    /**
    * \fn ClearScreen ()
    * \brief Function to delete the contents of the console.
    */
    void ClearScreen ()
    {
        cout << "\033[H\033[2J";
    } // ClearScreen ()
    
    /**
    * \fn Pause ()
    * \brief Function to generate one pause.
    */
    void Pause ()
    {
        string next;
        cout << "Enter character for continue : ";
        cin >> next;
    } // Pause ()

    /**
    * \fn SymbolAndColorChoise ()
    * \brief Function to select the symbol and color.
    */
    void SymbolAndColorChoise ()
    {
        for (unsigned i=0; i<NbPlayer; ++i)
        {
            ClearScreen ();
            
            while (true)
            {
                char rep;
                cout << "Player " << (i+1) << " have the symbol : \'" << SymbolPlayer[i]<< "\' " << endl;
                cout << "modify it ? (y/n) : ";
                cin >> rep;
                rep = tolower (rep);

                if ('y' == rep)
                {
                    char Symbol;
                    while (true)
                    {
                        cout << "Select your symbol (letter) : ";
                        cin >> Symbol;
                        
                        if((Symbol >= 'a' && Symbol <= 'z') || (Symbol >= 'A' && Symbol <= 'Z'))
                        {
                            bool SymbolIsFree = true;
                            
                            for(unsigned j=0; j<NbPlayer; ++j)
                                if(j != i && Symbol == SymbolPlayer[j])
                                {
                                    cout << "The player " << j+1 << " have the symbol !" << endl;
                                    SymbolIsFree = false;
                                    break;
                                }
                            
                            if(SymbolIsFree)
                                break;
                        }
                    }
                    
                    
                    SymbolPlayer[i] = Symbol;
                    break;
                }
                else if ('n' == rep)
                    break;
                ClearScreen();
            }

            while (true)
            {
                char rep;

                cout << "Player " << (i+1) << " have the " << ColorNamePlayer[i] << " color. " << endl;
                cout << "modify it ? (y/n) : ";
                cin >> rep;
                rep = tolower (rep);
                
                if ('y' == rep)
                {
                    cout << "30 :";
                    Color(KBlack);
                    cout << " Black" << endl;
                    Color(KReset);
                    cout << "31 :";
                    Color(KRed);
                    cout << " Red " << endl;
                    Color(KReset);
                    cout << "32 :";
                    Color(KGreen);
                    cout << " Green" << endl;
                    Color(KReset);
                    cout << "33 :";
                    Color(KYellow);
                    cout << " Yellow" << endl;
                    Color(KReset);
                    cout << "34 :";
                    Color(KBlue);
                    cout << " Blue" << endl;
                    Color(KReset);
                    cout << "35 :";
                    Color(KMagenta);
                    cout << " Magenta" << endl;
                    Color(KReset);
                    cout << "36 :";
                    Color(KCyan);
                    cout << " Cyan" << endl;
                    Color(KReset);
        
                    while (true)
                    {
                        string ChoiseColor;
                        cout << "Select symbol color: ";
                        cin >> ChoiseColor;
                        
                        if (ChoiseColor == KBlack || ChoiseColor == KRed || ChoiseColor == KGreen || ChoiseColor == KYellow || ChoiseColor == KBlue || ChoiseColor == KMagenta || ChoiseColor == KCyan)
                        {
                            ColorPlayer[i] = ChoiseColor;
                            ColorNamePlayer[i] = "Personalised";
                            break;
                        }
                    }
                    
                    break;
                }
                else if ('n' == rep)
                    break;
            }
        }
    } // SymbolAndColorChoise ()

    /**
    * \fn SpawnWall (CVMatrix & Mat)
    * \brief Function to spawn random wall in the game.
    * \param[in,out] Mat Matrix..
    */
    void SpawnWall (CVMatrix & Mat)
    {
        srand (time (NULL));
        unsigned Chance = rand() % 300;
        
        if (99 >= Chance)
        {
            while (true)
            {
                unsigned WallPosX = rand() % NbLine;
                unsigned WallPosY = rand() % NbColumn;
        
                if (Mat[WallPosX][WallPosY] == KEmpty)
                {
                    Mat[WallPosX][WallPosY] = KWall;
                    break;
                }
            }
        }
    } // SpawnWall ()

    /**
    * \fn KillPlayer (unsigned PosX, unsigned PosY, vector <CPosition> PosPlayers, CVMatrix & Mat)
    * \brief Function to kill players.
    * \param[in,out] Mat Matrix.
    * \param[in] PosX Line position of the player who play in the matrix.
    * \param[in] PosY Column position of the player who play in the matrix.
    * \param[in] PosPlayers position of the different players.
    */
    void KillPlayer (unsigned PosX, unsigned PosY, CVPosition PosPlayers, CVMatrix & Mat)
    {
        for (unsigned i=0; i<NbPlayer; ++i)
        {
            if (PosPlayers[i].first == PosX && PosPlayers[i].second == PosY)
            {
                VPlayerDead[i] = true;                
                --NbPlayerAlive;
                break;
            }
        }
    
        Mat[PosX][PosY] = KEmpty;
    } // KillPlayer ()


    /**
    * \fn ShowMatrix (const CVMatrix & Mat)
    * \brief Function to display the grid.
    * \param Mat Selected matrix
    */
    void ShowMatrix (const CVMatrix & Mat)
    {
        ClearScreen();
        Color(KReset);

        for (unsigned i=0; i<Mat.size (); ++i)
        {
            for (unsigned j=0; j<Mat[i].size (); ++j)
                cout << "+---";
            cout << "+" << endl;

            for (unsigned j=0; j<Mat[i].size (); ++j)
            {
                cout << "| ";
                if (Mat[i][j] == KEmpty || Mat[i][j] == KWall)
                    cout << Mat[i][j];
                else
                    for(unsigned k=0; k<NbPlayer; ++k)
                        if (Mat[i][j] == SymbolPlayer[k])
                        {
                            Color(ColorPlayer[k]);
                            cout << Mat[i][j];
                            Color(KReset);
                            break;
                        }
                cout << " ";
            }
            
            cout << "|" << endl;
        }

        for(unsigned j=0; j < Mat[0].size (); ++j)
            cout << "+---";
        cout << "+" << endl << endl;
    } // ShowMatrix ()

    /**
    * \fn InitMatFromFile (CVMatrix & Mat, const string & FileName, CPosition & PosPlayer1, CPosition & PosPlayer2)
    * \brief Function to initialize the grid.
    * \param[out] Mat Matrix
    * \param[out] PosPlayers Position of all players
    */
    void InitMatFromFile (CVMatrix & Mat, CVPosition & PosPlayers)
    {
        PosPlayers[0].first = 0;
        PosPlayers[0].second = NbLine-1;
        PosPlayers[1].first = NbColumn-1;
        PosPlayers[1].second = 0;
        PosPlayers[2].first = 0;
        PosPlayers[2].second = 0;
        PosPlayers[3].first = NbColumn-1;
        PosPlayers[3].second = NbLine-1;

        ifstream ifs ("config.cfg");
        if (ifs.is_open())
        {
            while (true)
            {
                string Config;
                ifs >> Config;
                if (ifs.eof ()) break;

                unsigned Value;
                ifs >> Value;
                if (ifs.eof ()) break;

                if (Config == "NbLine:")
                NbLine = Value;
                else if (Config == "NbCol:")
                NbColumn = Value;
                else if (Config == "XPosPlay1:")
                PosPlayers[0].first = Value;
                else if (Config == "YPosPlay1:")
                PosPlayers[0].second = Value;
                else if (Config == "XPosPlay2:")
                PosPlayers[1].first = Value;
                else if (Config == "YPosPlay2:")
                PosPlayers[1].second = Value;
                else if (Config == "XPosPlay3:")
                PosPlayers[2].first = Value;
                else if (Config == "YPosPlay3:")
                PosPlayers[2].second = Value;
                else if (Config == "XPosPlay4:")
                PosPlayers[3].first = Value;
                else if (Config == "YPosPlay4:")
                PosPlayers[3].second = Value;
            }

            if (NbLine < 2)
                NbLine = 10;

            if (NbColumn < 2)
                NbColumn = 10;


            if (PosPlayers[0].first > NbLine || PosPlayers[0].first < 0 || PosPlayers[0].second > NbLine || PosPlayers[0].second < 0)
            {
                PosPlayers[0].first = 0;
                PosPlayers[0].second = NbLine-1;
            }
            if (PosPlayers[1].first > NbLine || PosPlayers[1].first < 0 || PosPlayers[1].second > NbLine || PosPlayers[1].second < 0)
            {
                PosPlayers[1].first = NbLine-1;
                PosPlayers[1].second = 0;
            }
            if (NbPlayer > 2 && (PosPlayers[2].first > NbLine || PosPlayers[2].first < 0 || PosPlayers[2].second > NbLine || PosPlayers[2].second < 0))
            {
                PosPlayers[2].first = 0;
                PosPlayers[2].second = 0;

                if (NbPlayer == 4 && (PosPlayers[3].first > NbLine || PosPlayers[3].first < 0 || PosPlayers[3].second > NbLine || PosPlayers[3].second < 0))
                {
                    PosPlayers[3].first = NbColumn-1;
                    PosPlayers[3].second = NbLine-1;
                }
            }
        }
        ifs.close ();

        CVChar Line (NbLine, KEmpty);
        Mat.resize (NbColumn);
        
        for (unsigned i=0; i < Mat.size (); ++i)
            Mat[i] = Line;

        Mat[PosPlayers[0].first][PosPlayers[0].second] = SymbolPlayer[0];
        Mat[PosPlayers[1].first][PosPlayers[1].second] = SymbolPlayer[1];
        if (NbPlayer > 2)
        {
            Mat[PosPlayers[2].first][PosPlayers[2].second] = SymbolPlayer[2];
            if (NbPlayer == 4)
                Mat[PosPlayers[3].first][PosPlayers[3].second] = SymbolPlayer[3];
        }
        
        for (unsigned i=0 ; i< NbPlayerAlive ; ++i)
            VPlayerDead[i] = false;
    } // InitMatFromFile ()
    
    /**
    * \fn PlayerIsBlocked (const CVMatrix & Mat,const CVPosition & Pos,const unsigned PlayerPlaying)
    * \brief Function who return if player is blocked.
    * \param[in] Mat Matrix.
    * \param[in] Pos Position of all player.
    * \param[in] PlayerPlaying Player who plays.
    */
    bool PlayerIsBlocked (const CVMatrix & Mat,const CVPosition & Pos,const unsigned PlayerPlaying)
    {
        unsigned PosX = Pos[PlayerPlaying].first;
        unsigned PosY = Pos[PlayerPlaying].second;
        unsigned Bloqued = 0;
        
        if (PosX == 0 || PosY == 0 || KWall == Mat[PosX-1][PosY-1])
            ++Bloqued;
        if (PosX == 0 || KWall == Mat[PosX-1][PosY])
            ++Bloqued;
        if (PosX == 0 || PosY == NbLine-1 || KWall == Mat[PosX-1][PosY+1])
            ++Bloqued;
        if (PosY == 0 || KWall == Mat[PosX][PosY-1])
            ++Bloqued;
        if (PosY == NbColumn-1 || KWall == Mat[PosX][PosY+1])
            ++Bloqued;
        if (PosX == NbColumn-1 || PosY == 0 || KWall == Mat[PosX+1][PosY-1])
            ++Bloqued;
        if (PosX == NbColumn-1 || KWall == Mat[PosX+1][PosY])
            ++Bloqued;
        if (PosX == NbColumn-1 || PosY == NbLine-1 || KWall == Mat[PosX+1][PosY+1])
            ++Bloqued;
        
        if (Bloqued == 8)
            return true;
        else
            return false;
    } // PlayerIsBlocked ()

    /**
    * \fn MoveToken (CVMatrix & Mat, char Move, CPosition & Pos)
    * \brief Function of move token.
    * \param[in,out] Mat Matrix.
    * \param[in] Move Key movement.
    * \param[in,out] Pos Position of all player.
    * \param[in] PlayerPlaying Player who plays.
    */
    bool MoveToken (CVMatrix & Mat, char Move, CVPosition & Pos, unsigned PlayerPlaying)
    {

        unsigned PosX = Pos[PlayerPlaying].first;
        unsigned PosY = Pos[PlayerPlaying].second;

        if (Move == VPlayersKey[PlayerPlaying][0])
        {
            if (PosX == 0 || PosY == 0 || KWall == Mat[PosX-1][PosY-1])
                return false;
            else if (KEmpty != Mat[PosX-1][PosY-1])
                KillPlayer (PosX-1, PosY-1, Pos, Mat);

            swap(Mat[PosX-1][PosY-1], Mat[PosX][PosY]);
            --Pos[PlayerPlaying].first;
            --Pos[PlayerPlaying].second;
        }
        else if (Move == VPlayersKey[PlayerPlaying][1])
        {
            if (PosX == 0 || KWall == Mat[PosX-1][PosY])
                return false;
            else if (KEmpty != Mat[PosX-1][PosY])
                KillPlayer(PosX-1, PosY, Pos, Mat);

            swap(Mat[PosX-1][PosY], Mat[PosX][PosY]);
            --Pos[PlayerPlaying].first;
        }
        else if (Move == VPlayersKey[PlayerPlaying][2])
        {
            if (PosX == 0 || PosY == NbLine-1 || KWall == Mat[PosX-1][PosY+1])
                return false;
            else if (KEmpty != Mat[PosX-1][PosY+1])
                KillPlayer(PosX-1, PosY+1, Pos, Mat);

            swap(Mat[PosX-1][PosY+1], Mat[PosX][PosY]);
            --Pos[PlayerPlaying].first;
            ++Pos[PlayerPlaying].second;
        }
        else if (Move == VPlayersKey[PlayerPlaying][3])
        {
            if (PosY == 0 || KWall == Mat[PosX][PosY-1])
                return false;
            else if (KEmpty != Mat[PosX][PosY-1])
                KillPlayer(PosX, PosY-1, Pos, Mat);
    
            swap(Mat[PosX][PosY-1], Mat[PosX][PosY]);
            --Pos[PlayerPlaying].second;
        }
        else if (Move == VPlayersKey[PlayerPlaying][4])
        {
            if (PosY == NbColumn-1 || KWall == Mat[PosX][PosY+1])
                return false;
            else if (KEmpty != Mat[PosX][PosY+1])
                KillPlayer(PosX, PosY+1, Pos, Mat);

            swap(Mat[PosX][PosY+1], Mat[PosX][PosY]);
            ++Pos[PlayerPlaying].second;
        }
        else if (Move == VPlayersKey[PlayerPlaying][5])
        {
            if (PosX == NbColumn-1 || PosY == 0 || KWall == Mat[PosX+1][PosY-1])
                return false;
            else if (KEmpty != Mat[PosX+1][PosY-1])
                KillPlayer(PosX+1, PosY-1, Pos, Mat);

            swap(Mat[PosX+1][PosY-1], Mat[PosX][PosY]);
            ++Pos[PlayerPlaying].first;
            --Pos[PlayerPlaying].second;
        }
        else if (Move == VPlayersKey[PlayerPlaying][6])
        {
            if (PosX == NbColumn-1 || KWall == Mat[PosX+1][PosY])
                return false;
            else if (KEmpty != Mat[PosX+1][PosY])
                KillPlayer(PosX+1, PosY, Pos, Mat);

            swap(Mat[PosX+1][PosY], Mat[PosX][PosY]);
            ++Pos[PlayerPlaying].first;
        }
        else if (Move == VPlayersKey[PlayerPlaying][7])
        {
            if (PosX == NbColumn-1 || PosY == NbLine-1 || KWall == Mat[PosX+1][PosY+1])
                return false;
            else if (KEmpty != Mat[PosX+1][PosY+1])
                KillPlayer(PosX+1, PosY+1, Pos, Mat);

            swap(Mat[PosX+1][PosY+1], Mat[PosX][PosY]);
            ++Pos[PlayerPlaying].first;
            ++Pos[PlayerPlaying].second;
        }
        else
            return false;
    
        SpawnWall (Mat);
        return true;
    } // MoveToken ()

    /**
    * \fn int ppal ()
    * \brief Main function for playing.
    */
    int ppal()
    {
        CVMatrix Mat;
        CVPosition PosPlayers (4);
        
        unsigned NbRound = 20;
        unsigned PlayerPlaying = 1;
        
        SymbolAndColorChoise ();
        InitMatFromFile (Mat, PosPlayers);
        ShowMatrix (Mat);
            
        while (NbRound != 0)
        {
            char Move;
            
            bool Hmap = false;
            while (!Hmap)
            {
                if (PlayerIsBlocked(Mat, PosPlayers, PlayerPlaying-1))
                {
                    cout << "The player " << PlayerPlaying << " is blocked" << endl;
                    cout << "The player " << PlayerPlaying << " is killed" << endl;
                    VPlayerDead[PlayerPlaying-1] = true;
                    Mat[PosPlayers[PlayerPlaying-1].first][PosPlayers[PlayerPlaying-1].second] = KEmpty;
                    ++PlayerPlaying;
                    ShowMatrix (Mat);
                }
                if (false == VPlayerDead[PlayerPlaying-1])
                {
                    cout << "Remaining round : " << NbRound << endl;
        
                    system ("stty raw"); /// Ignore breaks signals
        
                    cout << "Enter move Player " << PlayerPlaying << " : ";
                    cin >> Move;

                    if (tolower(Move) == 'p') /// Exit signal
                    {
                        system ("stty cooked"); /// Listen breaks signals
                        cout << endl;
                        return 1;
                    }
        
                    Hmap = MoveToken (Mat, tolower(Move), PosPlayers, PlayerPlaying-1);
            
                    if (Hmap)
                    {
                        if (PlayerPlaying == NbPlayer)
                        {
                            PlayerPlaying = 1;
                            --NbRound;
                        }
                        else
                            ++PlayerPlaying;
                    }
        
                    system("stty cooked"); /// Listen breaks signals
                    ShowMatrix (Mat);
                }
                else
                    ++PlayerPlaying;
        
                if (NbPlayerAlive == 1)
                {
                    cout << "Player " << --PlayerPlaying << " Win !!!" << endl;
                    Pause();
                    return 0;
                }
                
                if (PlayerPlaying > NbPlayer)
                {
                    PlayerPlaying = 1;
                    ++NbRound;
                }
            }
        }
        Pause();
        return 0;
    } // ppal ()
    
    /**
    * \fn bool Keyboard (unsigned Player, char type, CVMatrix & VPlayersKey)
    * \brief Function of change the keybaord.
    * \param[in,out] Mat VPlayersKey.
    * \param[in] Type the difference type of keyboard.
    * \param[in] Player Player who select his keyboard.
    */
    bool KeyBoard (unsigned Player, const char Type, CVMatrix & VPlayersKey)
    {
        char choice;
        if (Type == '1')
        {
            VPlayersKey[Player] = {'a','z','e','q','d','w','x','c'};
            
            cout << "You have set your keys to play with a AZERTY keyboard!" << endl << endl;
            cout << "Recall of player" << Player+1 << " keys :" << endl << endl;
            for (int i=0; i<8; ++i)
                cout << setw(13) << left << KVNomTouch[i] << ":" << setw(2) << right << VPlayersKey[Player][i] << endl;

            while (true)
            {
                cout << endl;
                cout << "Are you satisfied? (y/n) ";
                cin >> choice;
            
                if (tolower(choice) == 'y')
                    return false;
                else if (tolower(choice) == 'n')
                    return true;
            }
        }
        else if (Type == '2')
        {
            VPlayersKey[Player] = {'q','w','e','a','d','z','x','c'};
            
            cout << "You have set your keys to play with a QWERTY keyboard!" << endl << endl;
            cout << "Recall of player" << Player+1 << " keys :" << endl << endl;
            for (int i=0; i < 8; ++i)
                cout << setw(13) << left << KVNomTouch[i] << ":" << setw(2) << right << VPlayersKey[Player][i] << endl;
            
            while (true)
            {
                cout << endl;
                cout << "Are you satisfied? (y/n) ";
                cin >> choice;
            
                if (tolower(choice) == 'y')
                    return false;
                else if (tolower(choice) == 'n')
                    return true;
            }
        }    
        else if (Type == '3')
        {
            VPlayersKey[Player] = {'7','8','9','4','6','1','2','3'};

            cout << "You have changed your keys to play with the numeric keypad!" << endl << endl;
            cout << "Recall of player" << Player+1 << " keys :" << endl << endl;
            for (int i=0; i < 8; ++i)
                cout << setw(13) << left << KVNomTouch[i] << ":" << setw(2) << right << VPlayersKey[Player][i] << endl;
    
            while (true)
            {
                cout << endl;
                cout << "Are you satisfied? (y/n) ";
                cin >> choice;
            
                if (tolower(choice) == 'y')
                    return false;
                else if (tolower(choice) == 'n')
                    return true;
            }
        }
        else if (Type == '4')
        {
            cout << "Customising of the key (except p): " << endl << endl;
            char TKey;
            
            for (int  i=0; i<8; ++i)
            {
                cout << KVNomTouch[i] << " : ";
                cin >> TKey;
                
                while (tolower(TKey) == 'p')
                {
                   cout << KVNomTouch[i] << " (except p) : ";
                   cin >> TKey;
                }
        
                VPlayersKey[Player][i] = TKey;
            }

            for (int i=0; i < 8; ++i)
                for(int j=i+1; j < 8; ++j)
                    if (VPlayersKey[Player][j]==VPlayersKey[Player][i])
                    {
                        cout << "You have set two different key for the same movement." << endl;
                        cout << "AZERTY configuration add" << endl; 
                        VPlayersKey[Player] = {'a','z','e','q','d','w','x','c'};
                    }
    
            cout << "Recall of player" << Player+1 << " keys :" << endl << endl;
    
            for (int j=0; j < 8;  ++j)
                cout << setw(13) << left << KVNomTouch[j] << ":" << setw(2) << right << VPlayersKey[Player][j] << endl;
    
            while (true)
            {
                cout << endl;
                cout << "Are you satisfied? (y/n) ";
                cin >> choice;
            
                if (tolower(choice) == 'y')
                    return false;
                else if (tolower(choice) == 'n')
                    return true;
            }
        }
        return false;
    } // KeyBoard ()

    /**
    * \fn ChNbPlayers ()
    * \brief Function to choose how mutch of player.
    */
    void ChNbPlayers()
    {
        ClearScreen();
        char NbPlayerCin;
        cout << "What's the number of players between 2 and 4 ? ";
        cin >> NbPlayerCin;
        
        while (NbPlayerCin < '2' || NbPlayerCin > '4')
        {
            cout << "Please type a integer number between 2 and 4 ";
            cin >> NbPlayerCin;
        }
        
        if (NbPlayerCin == '2')
            NbPlayer = 2;
        else if (NbPlayerCin == '3')
            NbPlayer = 3;
        else if (NbPlayerCin == '4')
            NbPlayer = 4;

        NbPlayerAlive = NbPlayer;
        
        cout << "You will play with " << NbPlayer << " players" << endl;
        cout << endl;
        
        ppal();
    } // ChNbPlayers ()
    
    /**
    * \fn Rules ()
    * \brief Function who show the rules.
    */
    void Rules ()
    {
        ClearScreen();
        cout << "---------- Presentation ----------" << endl;
        cout << endl;
        
        cout << "Token Royal is a game like a 'Catch me if you can' "<< endl;
        cout << "The game principle is to catch the other tokens" << endl;
        cout << "by moving on their position(like chess game)." << endl;
        cout << endl;
        
        cout << "----------- Game rules -----------" << endl;
        cout << endl;
        
        cout << "The rules are easy : " << endl;
        cout << "1. You're in a pit so you can't escape" << endl;
        cout << "2. There is a limited number of move, so running away is not a good idea" << endl;
        cout << "3. You can't move more than one time by turn" << endl;
        cout << "4. In versus game, the first who 'eat' the other player is the winner" << endl;
        cout << "5. In case of more than two players, the last token alive is the winner" << endl;
        cout << "6. Probality after player playing a spawn of wall represented by one " << KWall << endl;
        cout << "7. Enter p ingame for Rage Quit !" << endl;
        cout << endl;

        Pause();
    } // Rules ()

    /**
    * \fn KeyConfig ()
    * \brief Function to choose the keyboard.
    */
    void KeyConfig ()
    {
        for (unsigned i=0; i<NbPlayer; ++i)
        {
            bool choice = true;
            while (choice)
            {
                ClearScreen();
                char Type;
                cout << "For this game, you can use four different configurations :" << endl;
                cout << "1.Default azerty keyboard" << endl;
                cout << "2.Default qwerty keyboard" << endl;
                cout << "3.Default keypad" << endl;
                cout << "4.Personalised configuration" << endl;
                cout << endl;
                
                cout << "What kind of commands do you want to use ?" << endl;
                cout << endl;
                
                cout << "Player " << i+1 << " what is your choice ? ";
                cin >> Type;
                
                while (Type < '1' || Type > '4')
                {
                    cout << "Please type a integer number between 1 and 4 : ";
                    cin >> Type;
                }
                
                choice = KeyBoard (i , Type , VPlayersKey);
            }
        }
    } // KeyConfig ()
    
    /**
    * \fn Credits ()
    * \brief Function to show the credits.
    */
    void Credits ()
    {
        ClearScreen();
        cout << "Thanks for using this fabulous game" << endl;
        cout << "It has been designed and developed by this fabulous team :" << endl;
        cout << endl;
        cout << "Michael Gileta : 'The Elf'" << endl;
        cout << "Raphael Kumar : 'The Barbarian'" << endl;
        cout << "Kevin Duglue : 'The Minstrel'" << endl;
        cout << "Julien Jandot : 'The Dwarf'" << endl;
        cout << endl;
    } // Credits ()
    
    /**
    * \fn int Exit ()
    * \brief Function to exit the game.
    */
    bool Exit()
    {
        cout << "Are you sure ? (y/n) : ";
        
        char Quit;
        cin >> Quit;
        
        while (tolower(Quit) != 'y' && tolower(Quit) != 'n')
        {
            cout << "Press y to quit, n to cancel : ";
            cin >> Quit;
        }
        
        if (tolower(Quit) == 'y')
            return true;
        return false;
    } // Exit ()
    
    /**
    * \fn int Menu ()
    * \brief Main game program.
    */
    int Menu ()
    {
        while (true)
        {
            ClearScreen();
            
            bool ExitGame = false;
            cout << "Token Royal" << endl << endl;
            cout << "1. Start Game" << endl;
            cout << "2. Presentation/Rules" << endl;
            cout << "3. Keyboard Configuration" << endl;
            cout << "4. Credits" << endl;
            cout << "5. Quit" << endl;
            cout << endl;
        
            char Choice = 0;
            
            cout << "What do you want to do ? ";
            cin >> Choice;

            while (Choice < '1' || Choice > '5')
            {
                cout << "Please type a integer number between 1 and 5 : ";
                cin >> Choice;
            }
        
            switch (Choice)
            {
                case '1':
                    ChNbPlayers();
                break;
                case '2':
                    Rules();
                break;
                case '3':
                    KeyConfig();
                break;
                case '4':
                    Credits();
                    Pause();
                break;
                case '5':
                    ExitGame = Exit();
                    Credits();
                break;
            }    
            if (ExitGame)
                return 0;
        }
    } // Menu ()
    
} // namespace nsGame

/**
* \fn int main()
* \brief Main program.
*/
int main()
{
    nsGame::Menu();
    return 0;
} // main()
